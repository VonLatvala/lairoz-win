﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using uhttpsharp;
using uhttpsharp.RequestProviders;
using uhttpsharp.Listeners;
using System.Net.Sockets;
using System.Net;
using uhttpsharp.Handlers;

namespace LairOZ
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (ProcessIcon pi = new ProcessIcon())
            using (HttpServer httpServer = new HttpServer(new HttpRequestProvider()))
            {
                LairOZActionController lozac = new LairOZActionController();

                httpServer.Use(new TcpListenerAdapter(new TcpListener(IPAddress.Any, 8001)));
                httpServer.Use((context, next) => {
                    Console.WriteLine("Got Request!");
                    return next();
                });

                // Handler classes : 
                httpServer.Use(new HttpRouter().With(string.Empty, new IndexHandler())
                                                .With("monitor", new MonitorHandler(lozac.DoMonOn, lozac.DoMonOff, lozac.SetMonitorInternalState, lozac.GetMonitorInternalState))
                                                .With("volume", new VolumeHandler(lozac.DoVolInc, lozac.DoVolDec, lozac.DoVolSet, lozac.DoVolMute, lozac.DoVolUnmute, lozac.DoVolToggle, lozac.GetAudioStateJSON)));
                httpServer.Use(new ErrorHandler());

                httpServer.Start();

                pi.Display(lozac);

                // Make sure the application runs!
                Application.Run();
            }
        }
    }
}
