﻿using System;
using System.Text;
using System.Threading.Tasks;
using uhttpsharp;
using uhttpsharp.Headers;

namespace LairOZ
{
    public class IndexHandler : IHttpRequestHandler
    {
        private readonly HttpResponse _response;
        private readonly HttpResponse _keepAliveResponse;

        public IndexHandler()
        {
            byte[] contents = Encoding.UTF8.GetBytes("LairOZ ready.");
            _keepAliveResponse = new HttpResponse(HttpResponseCode.Ok, contents, true);
            _response = new HttpResponse(HttpResponseCode.Ok, contents, false);
        }

        public Task Handle(IHttpContext context, Func<Task> next)
        {
            context.Response = context.Request.Headers.KeepAliveConnection() ? _keepAliveResponse : _response;
            return Task.Factory.GetCompleted();
        }
    }
}