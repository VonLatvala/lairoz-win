﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LairOZ.Properties;

namespace LairOZ
{
    class ProcessIcon: IDisposable
    {
        NotifyIcon ni;

        public ProcessIcon()
        {
            ni = new NotifyIcon();
        }

        public void Dispose()
        {
            ni.Dispose();
        }

        /// <summary>
        /// Displays the icon in the system tray.
        /// </summary>
        public void Display(LairOZActionController lozac)
        {
            // Put the icon in the system tray and allow it react to mouse clicks.			
            ni.MouseClick += new MouseEventHandler(ni_MouseClick);
            ni.Icon = Resources.AppLogo;
            ni.Text = "LairOZ";
            ni.Visible = true;

            // Attach a context menu.
            ni.ContextMenuStrip = new ContextMenus().Create(lozac.DoMonOffEH(), lozac.DoMonOnEH(), lozac.DoVolMuteEH(), lozac.DoVolUnmuteEH(), lozac.DoVolToggleEH());
        }

        /// <summary>
        /// Handles the MouseClick event of the ni control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        void ni_MouseClick(object sender, MouseEventArgs e)
        {
            // Handle mouse button clicks.
            if (e.Button == MouseButtons.Left)
            {
                // Start Windows Explorer.
                Process.Start("explorer", null);
            }
        }

    }
}
