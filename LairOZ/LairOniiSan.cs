﻿using System;
using System.Runtime.InteropServices;
using AudioSwitcher.AudioApi.CoreAudio;
using System.Diagnostics;
using System.Windows;

namespace LairOZ
{
    public class LairOZActionController
    {
        private const int HWND_BROADCAST = 0xFFFF;

        //top-level windows in the system    
        private const int SC_MONITORPOWER = 0xF170;
        private const int WM_SYSCOMMAND = 0x112;

        private const int MONITOR_ON = -1;
        private const int MONITOR_OFF = 2;
        private const int MONITOR_STANDBY = 1;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SendNotifyMessage(int hWnd, int hMsg, int wParam, int lParam);

        private bool curMonPower = true;
        private CoreAudioDevice defaultPlaybackDevice;


        public LairOZActionController()
        {
            defaultPlaybackDevice = new CoreAudioController().DefaultPlaybackDevice;
        }

        public string GetAudioState()
        {
            return defaultPlaybackDevice.Volume + ", " + (defaultPlaybackDevice.IsMuted ? "1" : "0");
        }

        public string GetAudioStateJSON()
        {
            return "{\"vol\": " + defaultPlaybackDevice.Volume + ", \"muted\": " + (defaultPlaybackDevice.IsMuted ? "true" : "false") + "}";
        }

        internal EventHandler DoVolToggleEH()
        {
            Action<object, EventArgs> h = delegate { DoVolToggle(); };
            return new EventHandler(h);
        }

        internal EventHandler DoVolUnmuteEH()
        {
            Action<object, EventArgs> h = delegate { DoVolUnmute(); };
            return new EventHandler(h);
        }

        internal EventHandler DoVolMuteEH()
        {
            Action<object, EventArgs> h = delegate { DoVolMute(); };
            return new EventHandler(h);
        }

        internal EventHandler DoMonOnEH()
        {
            Action<object, EventArgs> h = delegate { DoMonOn(); };
            return new EventHandler(h);
        }

        internal EventHandler DoMonOffEH()
        {
            Action<object, EventArgs> h = delegate { DoMonOff(); };
            return new EventHandler(h);
        }

        public int RunCommand(string cmd, string[] inParams)
        {
            switch (cmd)
            {
                case "monitor":
                    if(inParams.Length < 1)
                    {
                        Console.WriteLine("Not enough params for monitor");
                        return 2;
                    }
                    switch (inParams[0])
                    {
                        case "state":
                            Console.WriteLine("Monitor power state: " + (curMonPower == true ? "On" : "Off"));
                            break;
                        case "on":
                            Console.WriteLine("Turning monitor ON");
                            DoMonOn();
                            break;
                        case "off":
                            Console.WriteLine("Turning monitor OFF");
                            DoMonOff();
                            break;
                        case "standby":
                            Console.WriteLine("Putting monitor into STANDBY");
                            DoMonStandby();
                            break;
                        default:
                            Console.WriteLine("Couldn't understand " + inParams[0]);
                            break;
                    }
                    break;
                case "vol":
                    if(inParams.Length < 1)
                    {
                        Console.WriteLine("Not enough params for vol");
                        return 2;
                    }
                    switch (inParams[0])
                    {
                        case "state":
                            Console.WriteLine(GetAudioState());
                            break;
                        case "set":
                            DoVolSet(Int32.Parse(inParams[1]));
                            Console.WriteLine(GetAudioState());
                            break;
                        case "inc":
                            DoVolInc(Int32.Parse(inParams[1]));
                            Console.WriteLine(GetAudioState());
                            break;
                        case "dec":
                            DoVolDec(Int32.Parse(inParams[1]));
                            Console.WriteLine(GetAudioState());
                            break;
                        case "unmute":
                            DoVolUnmute();
                            Console.WriteLine(GetAudioState());
                            break;
                        case "mute":
                            DoVolMute();
                            Console.WriteLine(GetAudioState());
                            break;
                        case "toggle":
                            DoVolToggle();
                            Console.WriteLine(GetAudioState());
                            break;
                        default:
                            Console.WriteLine("Couldn't understand " + inParams[0]);
                            break;
                    }
                    break;
                default:
                    Console.WriteLine("unrecognized command '" + cmd + "'");
                    break;
            }
            return 0;
        }

        public void DoMonOff()
        {
            SendNotifyMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, MONITOR_OFF);
            SetMonitorInternalState(false);
        }

        public void DoMonOn()
        {
            SendNotifyMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, MONITOR_ON);
            SetMonitorInternalState(true);
        }

        public void DoMonStandby()
        {
            SendNotifyMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, MONITOR_STANDBY);
        }

        public void DoVolSet(Int32 amount)
        {
            defaultPlaybackDevice.Volume = amount;
        }

        public void DoVolMute()
        {
            defaultPlaybackDevice.Mute(true);
        }

        public void DoVolUnmute()
        {
            defaultPlaybackDevice.Mute(false);
        }

        public void DoVolToggle()
        {
            defaultPlaybackDevice.Mute(!defaultPlaybackDevice.IsMuted);
        }

        public void DoVolInc(Int32 amount)
        {
            defaultPlaybackDevice.Volume += amount;
        }

        public void DoVolDec(Int32 amount)
        {
            defaultPlaybackDevice.Volume -= amount;
        }

        public void SetMonitorInternalState(bool isOn)
        {
            curMonPower = isOn;
        }

        public bool GetMonitorInternalState()
        {
            return curMonPower;
        }
    }
}