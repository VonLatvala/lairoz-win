﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uhttpsharp;
using uhttpsharp.Headers;

namespace LairOZ
{
    public class MonitorHandler : IHttpRequestHandler
    {
        public delegate void GenericAction();
        public delegate void GenericOneBoolAction(Boolean x);
        public delegate bool GenericBoolQueryFn();

        GenericAction DoMonitorOn;
        GenericAction DoMonitorOff;
        GenericOneBoolAction SetInternalState;
        GenericBoolQueryFn GetInternalMonitorState;

        public MonitorHandler(GenericAction doOn, GenericAction doOff, GenericOneBoolAction setInternalState, GenericBoolQueryFn getInternalState)
        {
            DoMonitorOn = doOn;
            DoMonitorOff = doOff;
            SetInternalState = setInternalState;
            GetInternalMonitorState = getInternalState;
        }

        public Task Handle(IHttpContext context, Func<Task> next)
        {
            String responseTxt = "{\"power\": ";
            var requestedMode = context.Request.QueryString.GetByNameOrDefault("set", "");
            Console.WriteLine("POST BODY:\n" + context.Request.Post.Raw.ToString());
            switch (requestedMode)
            {
                case "on":
                    DoMonitorOn();
                    responseTxt += "true}";
                    break;
                case "off":
                    DoMonitorOff();
                    responseTxt += "false}";
                    break;
                case "setInternalState":
                    var state = context.Request.QueryString.GetByNameOrDefault("val", "") == "1" ? true : false;
                    SetInternalState(state);
                    responseTxt += (state == true ? "1" : "0")+"}";
                    //Console.WriteLine(responseTxt);
                    break;
                default:
                    // Get state
                    Boolean curState = GetInternalMonitorState();
                    responseTxt += (curState == true ? "true" : "false") + "}";
                    break;
            }
            byte[] contents = Encoding.UTF8.GetBytes(responseTxt);
            //Console.WriteLine("Requesting monitor mode: " + requestedMode);
            HttpResponse _keepAliveResponse = new HttpResponse(HttpResponseCode.Ok, contents, true);
            HttpResponse _response = new HttpResponse(HttpResponseCode.Ok, contents, false);
            context.Response = context.Request.Headers.KeepAliveConnection() ? _keepAliveResponse : _response;
            return Task.Factory.GetCompleted();
        }
    }
}