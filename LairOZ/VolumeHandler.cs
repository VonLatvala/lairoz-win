﻿using System;
using System.Text;
using System.Threading.Tasks;
using uhttpsharp;
using uhttpsharp.Headers;

namespace LairOZ
{
    public class VolumeHandler : IHttpRequestHandler
    {
        public delegate void GenericAction();
        public delegate void OneInt32Function(Int32 a);
        public delegate string GenericFunction();

        OneInt32Function DoVolInc;
        OneInt32Function DoVolDec;
        OneInt32Function DoVolSet;
        GenericAction DoVolMute;
        GenericAction DoVolUnmute;
        GenericAction DoVolToggle;
        GenericFunction GetVolumeStateJSON;

        public VolumeHandler(OneInt32Function InDoVolInc, OneInt32Function InDoVolDec, OneInt32Function InDoVolSet, GenericAction InDoVolMute, GenericAction InDoVolUnmute, GenericAction InDoVolToggle, GenericFunction InGetVolumeStateJSON)
        {
            DoVolInc = InDoVolInc;
            DoVolDec = InDoVolDec;
            DoVolSet = InDoVolSet;
            DoVolMute = InDoVolMute;
            DoVolUnmute = InDoVolUnmute;
            DoVolSet = InDoVolSet;
            DoVolToggle = InDoVolToggle;
            GetVolumeStateJSON = InGetVolumeStateJSON;
        }

        public Task Handle(IHttpContext context, Func<Task> next)
        {
            String responseTxt = "Volume: ";
            var requestedMode = context.Request.QueryString.GetByNameOrDefault("action", "");
            Int32 val = context.Request.QueryString.GetByNameOrDefault("val", 5);
            switch (requestedMode)
            {
                case "inc":
                    DoVolInc(val);
                    responseTxt += "INC " + val;
                    break;
                case "dec":
                    DoVolDec(val);
                    responseTxt += "DEC " + val;
                    break;
                case "set":
                    DoVolSet(val);
                    responseTxt += "SET " + val;
                    break;
                case "mute":
                    DoVolMute();
                    responseTxt += "MUTE";
                    break;
                case "unmute":
                    DoVolUnmute();
                    responseTxt += "UNMUTE";
                    break;
                case "toggle":
                    DoVolToggle();
                    responseTxt += "TOGGLE";
                    break;
                default:
                    responseTxt += "INVALID";
                    break;
            }

            //byte[] contents = Encoding.UTF8.GetBytes(responseTxt);
            byte[] contents = Encoding.UTF8.GetBytes(GetVolumeStateJSON());
            Console.WriteLine("Requesting volume mode: " + requestedMode);
            HttpResponse _keepAliveResponse = new HttpResponse(HttpResponseCode.Ok, contents, true);
            HttpResponse _response = new HttpResponse(HttpResponseCode.Ok, contents, false);
            context.Response = context.Request.Headers.KeepAliveConnection() ? _keepAliveResponse : _response;
            return Task.Factory.GetCompleted();
        }
    }
}
