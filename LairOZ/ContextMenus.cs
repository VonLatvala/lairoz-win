﻿#define UNPROFESSIONAL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using LairOZ.Properties;
using System.Drawing;

namespace LairOZ
{
    class ContextMenus
    {

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>ContextMenuStrip</returns>
        public ContextMenuStrip Create(EventHandler MonOff, EventHandler MonOn, EventHandler VolMute, EventHandler VolUnmute, EventHandler VolToggle)
        {
            // Add the default menu options.
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem item;
            ToolStripSeparator sep;

            item = new ToolStripMenuItem();
            item.Text = "Monitor On";
            item.Click += MonOn;
            menu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = "Monitor Off";
            item.Click += MonOff;
            menu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = "Mute Volume";
            item.Click += VolMute;
            menu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = "Unmute Volume";
            item.Click += VolUnmute;
            menu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = "Toggle Volume";
            item.Click += VolToggle;
            menu.Items.Add(item);

            item = new ToolStripMenuItem();
            item.Text = "Monitor On";
            item.Click += new EventHandler(MonOn);
            menu.Items.Add(item);

            #if UNPROFESSIONAL
            item = new ToolStripMenuItem();
            item.Text = "Oispa";
            string b = string.Empty;foreach (char a in "otyzgmx.gs/robkpg") {if (!char.IsLetter(a)){b += a;} else{char d = char.IsUpper(a) ? 'A' : 'a';b += (char)((((a + 20) - d) % 26) + d);}}
            item.Click += new EventHandler(delegate(object sender, EventArgs e) { Process.Start("http://"+b);});
            menu.Items.Add(item);
            #endif

            /*// About.
            item = new ToolStripMenuItem();
            item.Text = "About";
            item.Click += new EventHandler(About_Click);
            //item.Image = Resources.About;
            menu.Items.Add(item);*/

            // Separator.
            sep = new ToolStripSeparator();
            menu.Items.Add(sep);

            // Exit.
            item = new ToolStripMenuItem();
            item.Text = "Exit";
            item.Click += new System.EventHandler(Exit_Click);
            //item.Image = Resources.Exit;
            menu.Items.Add(item);

            return menu;
        }

        /// <summary>
        /// Handles the Click event of the Explorer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
/*        void Explorer_Click(object sender, EventArgs e)
        {
            Process.Start("explorer", null);
        }*/

        /// <summary>
        /// Handles the Click event of the About control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /*void About_Click(object sender, EventArgs e)
        {
            if (!isAboutLoaded)
            {
                isAboutLoaded = true;
                new AboutBox().ShowDialog();
                isAboutLoaded = false;
            }
        }*/

        /// <summary>
        /// Processes a menu item.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void Exit_Click(object sender, EventArgs e)
        {
            // Quit without further ado.
            Application.Exit();
        }
    }
}
