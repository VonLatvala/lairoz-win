# LairOZ-Win

## What is this?

A windows daemon which controls and reports volume and monitor power. Used in conjunction with Homebridge:

https://bitbucket.org/VonLatvala/homebridge-lairoz-speaker-plugin/src/master/

https://bitbucket.org/VonLatvala/homebridge-lairoz-monitor-plugin/src/master/

## Known issues

Opening an RDP connection to a computer running this software will upset the monitor controlling library, as RDP uses a virtual monitor. This will in turn result in the software eating all RAM available. Last time I checked no updates were available to the monitor controlling library.
